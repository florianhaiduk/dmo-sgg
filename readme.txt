=== DMO Spacer Gif Generator ===
Contributors: dasmoment
Donate link: https://dasmoment.de/dmo-sgg
Tags: images, attachment, spacer, placeholder, gif, spacerimages, speed, grid, lazyloading
Requires at least: 4.7.1
Tested up to: 4.9.6
Stable tag: 1.2.1
Requires PHP: 5.6
Text Domain: dmo-sgg

== Description ==

Whether building a **masonry grid**, using **lazyloading** or trying to **increase pagespeed**, sometimes it makes sense to use a **gif as a spacer**. The **DMO Spacer Gif Generator** generates automatically a monochrome (white) gif from ervery uploaded image and every media size. Also you can generate manually pending spacer gifs or regenrate the hole medialibrary.


= Links =
* [Contributors](https://dasmoment.de)
* [Documentation](https://dasmoment.de/dmo-sgg)


== Installation ==

1. Download the plugin and uploade the files to the wordpress plugins folder.
2. Activate the plugin through the 'Plugins' menu in WordPress.


== Screenshots == 

1. Choose if you want to generate all or just the pending spacer gifs and click on **Generate**


== Usage ==

1. Once you have installed and activated the plugin, it will automatically generate the requested spacer gifs.
2. The DMO SGG generates 10 pending spacer gifs on every post save
3. Generate spacer gifs manually in -> Settings -> Generate Spacer
4. Read the documentaion to [get started](https://dasmoment.de/dmo-sgg)


== Changelog ==

= 1.2.1 =
* Bugfix

= 1.2.0 =
* German translation added

= 1.1.2 =
* Description changed

= 1.1.1 =
* Unused function removed

= 1.1.0 =
* The DMO SGG now generates 10 pending spacer gifs on every post save
* The backend text output is translatable now
* Unsused code removed

= 1.0.1 =
* First Release


== Upgrade Notice ==

= 1.2.1 =
Small bugfix

= 1.2.0 =
German translation added

= 1.1.2 =
Better description

= 1.1.1 =
The DMO SGG now generates 10 pending spacer gifs on every post save

= 1.1.0 =
The DMO SGG now generates 10 pending spacer gifs on every post save

= 1.0.1 =
First Release