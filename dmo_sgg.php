<?php

/*
 * Plugin Name: DMO Spacer Gif Generator
 * Plugin URI: https://dasmoment.de/dmo-sgg
 * Description: The DMO Spacer Gif Generator genrates automatically a monochrome (white) gif from ervery uploaded image and every media size. Also you can generate manually pending spacer gifs or regenrate the hole medialibrary.
 * Version: 1.2.1
 * Author: DAS MOMENT
 * Author URI: https://dasmoment.de
 * License: GPLv3
 * Text Domain: dmo-sgg
 */
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Helper functions
 */
include_once 'classes/dmo_sgg_helper.class.php';
/**
 * Plugin template
 */
include_once 'classes/dmo_sgg_options.class.php';
/**
 * Check if images exists
 */
include_once 'classes/dmo_sgg_check_image.class.php';
/**
 * Genrate spacer gif if images
 */
include_once 'classes/dmo_sgg_generator.class.php';

/**
 * Create images folder
 */
function dmo_sgg_on_activation()
{
    $helper = new DmoSggHelper();
    $helper->dmo_sgg_createFolder();
}
register_activation_hook(__FILE__, 'dmo_sgg_on_activation');
/**
 * Load translation files
 */
function plugin_init()
{
    load_plugin_textdomain('dmo-sgg', false, basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'plugin_init');
/**
 * Load required scripts ans styles on plugin initalisation
 */
function dmo_sgg_initSrc()
{
    wp_enqueue_style('styles', plugins_url('assets/css/styles.min.css', __FILE__));
    wp_enqueue_script('script', plugin_dir_url(__FILE__) . 'assets/js/script.min.js', array('jquery'));
}
add_action('admin_enqueue_scripts', 'dmo_sgg_initSrc');

/**
 * Load plugin settings page if user is admin
 */
if (is_admin()) {
    $options = new DmoSggGenerateSpacerImageOptions();
}

/**
 * Generate 10 pending spacer gifs on every post save
 */
function dmo_sgg_generator_on_save()
{
    $check = new DmoSggCheckIfImageExists();
    $generator = new DmoSggGenerateSpacerImage();
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
    );
    $imagesArr = get_posts($args);
    $total = count($imagesArr);
    foreach ($imagesArr as $imageKey => $image) {
        if ($check->dmo_sgg_check($image->ID) == true) {
            unset($imagesArr[$imageKey]);
        }
    }
    $i = 0;
    foreach ($imagesArr as $imageKey => $image) {
        $i++;
        $generator->dmo_sgg_generateSpacerImg($image->ID);
        unset($imagesArr[$imageKey]);
        if ($i == 9) {
            break;
        }
    }
}
add_action('save_post', 'dmo_sgg_generator_on_save');

/**
 * Process ajax request
 */
function dmo_sgg_generator()
{
    $check = new DmoSggCheckIfImageExists();
    $generator = new DmoSggGenerateSpacerImage();
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
    );
    $imagesArr = get_posts($args);
    $total = count($imagesArr);
    $bulk = $_POST['bulk'];
    $checkPending = $_POST['check'];
    if ($bulk == 0) {
        foreach (glob(WP_CONTENT_DIR . '/dmo-sgg/' . '*.*') as $v) {
            unlink($v);
        }
        $results = [
            'total' => count($imagesArr),
            'pending' => count($imagesArr),
            'percent' => 0,
            'all_deleted' => true,
        ];
        echo json_encode($results);
        die();
    } else if ($checkPending == 1) {
        foreach ($imagesArr as $imageKey => $image) {
            if ($check->dmo_sgg_check($image->ID) == true) {
                unset($imagesArr[$imageKey]);
            }
        }
        $results = [
            'total' => $total,
            'pending' => count($imagesArr),
            'percent' => round(100 - ((100 / $total) * count($imagesArr)), 0),
            'all_deleted' => false,
        ];
        echo json_encode($results);
        die();
    } else {
        foreach ($imagesArr as $imageKey => $image) {
            if ($check->dmo_sgg_check($image->ID) == true) {
                unset($imagesArr[$imageKey]);
            }
        }
        $i = 0;
        foreach ($imagesArr as $imageKey => $image) {
            $i++;
            $generator->dmo_sgg_generateSpacerImg($image->ID);
            unset($imagesArr[$imageKey]);
            if ($i == 4) {
                break;
            }
        }
        $results = [
            'total' => $total,
            'pending' => count($imagesArr),
            'percent' => round(100 - ((100 / $total) * count($imagesArr)), 0),
            'all_deleted' => false,
        ];
        echo json_encode($results);
        die();
    }
}
add_action('wp_ajax_dmo_sgg_generator', 'dmo_sgg_generator');

/**
 * Theme function to get an specific spacer gif by ID and Size
 */
function dmo_sgg_get_spacer_img($id, $size)
{
    if ($id) {
        $check = new DmoSggCheckIfImageExists();
        $generator = new DmoSggGenerateSpacerImage();
        if ($check->dmo_sgg_check($id) == true) {
            return content_url() . '/dmo-sgg/' . $id . '-spacer-' . $size . '.gif';
        } else {
            $generator->dmo_sgg_generateSpacerImg($id);
            return content_url() . '/dmo-sgg/' . $id . '-spacer-' . $size . '.gif';
        }
    }
}
