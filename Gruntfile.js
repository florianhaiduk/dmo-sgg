module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  var globalConfig = {
    css: 'css',
    scss: 'scss',
    js: 'js',
    mod: 'node_modules',
    dist: 'assets',
    build: '../dmo/public_html/wp-content/plugins/dmo-sgg'
  };

  // Project configuration.
  grunt.initConfig({
    globalConfig: globalConfig,
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      src: {
        files: [
          '<%= globalConfig.js  %>/*.js',
          '<%= globalConfig.scss  %>/*.scss',
          'classes/*.php',
          'languages/*',
          '*.php',
          '*.txt',
        ],
        tasks: ['default'],
        options: {
          spawn: false
        }
      }
    },
    copy: {
      task1: {
        files: [{
          expand: true,
          flatten: true,
          dot: true,
          //cwd: 'src',
          dest: '<%= globalConfig.dist  %>/js/',
          src: [
            '<%= globalConfig.js  %>/project.js'
          ],
        }]
      },
      task2: {
        files: [{
          expand: true,
          flatten: false,
          dot: true,
          //cwd: 'src',
          dest: '<%= globalConfig.build  %>',
          src: [
            'classes/**',
            'assets/**',
            'languages/**',
            '*.php',
            'readme.txt',
          ],
        }]
      },
    },
    jshint: {
      all: ['Gruntfile.js', '<%= globalConfig.js  %>/*js']
    },
    sasslint: {
      options: {
        configFile: '.sass-lint.yml',
      },
      target: ['<%= globalConfig.scss  %>/*.scss']
    },
    sass: {
      options: {
        sourcemap: 'none',
        style: 'compressed'
      },
      dist: {
        files: {
          '<%= globalConfig.dist %>/css/styles.css': '<%= globalConfig.scss  %>/project.scss',
        }
      }
    },
    cssmin: {
      task1: {
        files: {
          '<%= globalConfig.dist %>/css/styles.css': '<%= globalConfig.dist %>/css/styles.css'
        }
      },
      task2: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.dist %>/css/',
          src: ['*.css', '!*.min.css'],
          dest: '<%= globalConfig.dist %>/css/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      task: {
        options: {
          sourceMap: true,
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
          //mangle: true
        },
        files: {
          '<%= globalConfig.dist  %>/js/script.min.js': '<%= globalConfig.dist  %>/js/*.js'
        }
      }
    },
    clean: {
      options: {
        force: true
      },
      task1: [
        '<%= globalConfig.dist %>/js/*.js'
      ],
      task2: [
        '<%= globalConfig.dist %>/css/*.css',
        '!<%= globalConfig.dist %>/css/*.min.css'
      ],
      build: [
        '<%= globalConfig.build %>/*'
      ]
    }
  });

  // Default tasks
  grunt.registerTask('default', ['jshint', 'sasslint', 'clean:task1', 'copy:task1', 'sass', 'cssmin', 'uglify', 'clean:task2', 'clean:build', 'copy:task2']);
};
