var bulk = null;
var check = null;

function regenerate() {
  if (bulk != null || check != 0) {
    var data = {
      'action': 'dmo_sgg_generator',
      'bulk': bulk,
      'check': check
    };
    jQuery.ajax({
      method: "POST",
      url: "/wp-admin/admin-ajax.php",
      data: data
    })
      .done(function (results) {
        var data = jQuery.parseJSON(results);
        if (data.pending != 0) {
          if (data.all_deleted == true) {
            bulk = 1;
          }
          regenerate();
          jQuery('#progress span').css({
            'width': data.percent + '%',
            'background': 'rgba(0, 133, 186, 1)'
          });
          jQuery('#progress-text').text(data.percent + '%');
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top .count').text(data.pending);
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top').hide(0);
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top-1').show(0);
        } else {
          jQuery('#progress span').css({
            'width': '100%',
            'background': '#46b450'
          });
          jQuery('#progress-text').text('Finished');
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top .count').text(data.total);
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top').hide(0);
          jQuery('#dmo-sgg-notices-top .dmo-sgg-notice-top-2').show(0);
        }
      })
      .fail(function (msg) {
        console.log('Error: ' + msg.responseText);
      });
  }
}

function checkOptions() {
  var radio = jQuery("#bulk-options :radio:checked").val();
  switch (radio) {
    case 'all':
      bulk = 0;
      jQuery('#generate').removeClass('button-disabled');
      jQuery('#dmo-sgg-notices-bottom .dmo-sgg-notice-bottom').hide(0);
      break;
    case 'pending':
      bulk = 1;
      jQuery('#generate').removeClass('button-disabled');
      jQuery('#dmo-sgg-notices-bottom .dmo-sgg-notice-bottom').hide(0);
      break;
    default:
      jQuery('#dmo-sgg-notices-bottom .dmo-sgg-notice-bottom').hide(0);
      jQuery('#dmo-sgg-notices-bottom .dmo-sgg-notice-bottom-1').show(0);
      break;
  }
  jQuery('#generator-options').removeClass('hidden');
}


jQuery(window).load(function () {

  // check generator options
  checkOptions();

  jQuery('#generate').click(function () {
    checkOptions();
    check = null;
    regenerate();
  });

  jQuery('#bulk-options input').click(function () {
    check = null;
    checkOptions();
  });


});