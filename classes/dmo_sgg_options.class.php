<?php
if (!defined('ABSPATH')) {
    exit;
}

class DmoSggGenerateSpacerImageOptions
{
    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    public function add_plugin_page()
    {
        add_options_page(
            'Settings Admin',
            'Generate Spacer Gifs',
            'manage_options',
            'dmo-spacer-gif-generator',
            array($this, 'create_admin_page')
        );
    }

    public function page_init()
    {}

    public function dmo_sgg_status()
    {
        $output = null;

        if (!is_dir(WP_CONTENT_DIR . '/dmo-sgg')) {
            $output .= '<li>Images folder: <span class="red">' . __('Could not be created', 'dmo-sgg') . '</span></li>';
        }

        if ($output != null) {
            return '<div class="general"><h2>' . __('Status', 'dmo-sgg') . '</h2><hr /><ul>' . $output . '</ul></div>';
        }
    }

    public function create_admin_page()
    {
        ?>
        <div class="wrap">
            <h1>DMO Spacer Gif Generator</h1><br />
            <?php echo $this->dmo_sgg_status(); ?>
            <h2><?php echo __('Generate gifs:', 'dmo-sgg'); ?></h2>
            <div id="progress" class="progress-bar">
              <span style="width:0%;">
              <span id="progress-text" class="progress-text">0%</span>
              </span>
            </div>
            <br />
            <div class="info">
              <span class=""><span><?php echo array_sum((array) wp_count_attachments($mime_type = 'image')); ?></span> <?php echo __('Images Total', 'dmo-sgg'); ?> </span><br />
              <div id="dmo-sgg-notices-top" class="dmo-sgg-notices dmo-sgg-notices-top"> 
                <span class="dmo-sgg-notice dmo-sgg-notice-top dmo-sgg-notice-top-1 red"><span class="count"></span>&nbsp;<?php echo __('Images pending', 'dmo-sgg'); ?></span>
                <span class="dmo-sgg-notice dmo-sgg-notice-top dmo-sgg-notice-top-2 green"><span class="count"></span>&nbsp;<?php echo __('Images generated', 'dmo-sgg'); ?></span>
              </div>
            </div>
            <br />
            <div id="generator-options" class="generator-options hidden">
              <fieldset id="bulk-options">
                <label>
                  <input type="radio" name="type" value="all">
                  <?php echo __('Regenerate <strong>all</strong> spacer gifs. This may take a while (Do not close the window).', 'dmo-sgg'); ?>
                </label><br />
                <label>
                  <input type="radio" name="type" value="pending">
                  <?php echo __('Generate pending spacer gifs (Do not close the window).', 'dmo-sgg'); ?>
                </label>
              </fieldset>
              <br />
              <div id="dmo-sgg-notices-bottom" class="dmo-sgg-notices dmo-sgg-notices-bottom">
                <div class="dmo-sgg-notice dmo-sgg-notice-bottom dmo-sgg-notice-bottom-1 red"><?php echo __('Please select an option', 'dmo-sgg'); ?></div>
              </div>
              <br />
              <button class="button button-primary button-disabled" id="generate"><?php echo __('Generate', 'dmo-sgg'); ?></button>
              <br />
            </div>
        </div>
      <?php
    }
}