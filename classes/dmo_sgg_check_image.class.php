<?php
if (!defined('ABSPATH')) {
    exit;
}

class DmoSggCheckIfImageExists extends DmoSggHelper
{
    public function __construct($attachment_id = false)
    {
        if ($attachment_id) {
            $this->dmo_sgg_check($attachment_id);
        }
    }

    public function dmo_sgg_check($attachment_id)
    {
        $imgArr = wp_get_attachment_metadata($attachment_id);
        foreach ($imgArr['sizes'] as $key => $size) {
            if (file_exists($this->dmo_sgg_getSpacerImagePath($attachment_id, $key))) {
                return $this->dmo_sgg_getSpacerImagePath($attachment_id, $key);
            } else {
                return false;
            }
        }
    }
}
