<?php
if (!defined('ABSPATH')) {
    exit;
}

class DmoSggGenerateSpacerImage extends DmoSggHelper
{

    public function __construct($id = false)
    {
        if ($id) {
            $this->dmo_sgg_generateSpacerImg($id);
        }
    }

    public function dmo_sgg_generateSpacerImg($id)
    {
        $this->dmo_sgg_createFolder();
        $type = get_post_mime_type($id);
        if ($type != 'application/pdf') {
            $imgArr = wp_get_attachment_metadata($id);
            foreach ($imgArr['sizes'] as $key => $size) {
                $img = imagecreatetruecolor($size['width'], $size['height']);
                $color = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
                imagefill($img, 0, 0, $color);
                imagegif($img, WP_CONTENT_DIR . '/dmo-sgg/' . $id . '-spacer-' . $key . '.gif');
            }
        }
    }
}
