<?php
if (!defined('ABSPATH')) {
    exit;
}

class DmoSggHelper
{

    public function __construct()
    {

    }

    public function dmo_sgg_getSpacerImagePath($id = false, $size = false)
    {
        $imgArr = wp_get_attachment_metadata($id);
        $file = WP_CONTENT_DIR . '/dmo-sgg/' . $id . '-spacer-' . $size . '.gif';
        return $file;
    }

    public function dmo_sgg_createFolder()
    {
        $folder = WP_CONTENT_DIR . '/dmo-sgg';
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
    }
}
